/////////////////////Завдання 1////////////////////////////////////////

function avg(x, y, z, w) {
    return (x + y + z + w) / arguments.length
}

console.log(avg(10, 4, 17, 6));

/////////////////////Завдання 2////////////////////////////////////////


function avg1(...numbers) {
    let sum = 0;
    for (let i = 0; i < arguments.length; i++) {
        sum += arguments[i]
    }

    return sum / arguments.length
}

console.log(avg1(1, 3, 3, 3));

/////////////////////Завдання 3////////////////////////////////////////

function square(a, b, c) {
    if (a + b >= c && a + c >= b && b + c >= a) {
        let p = (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c))
    }
    return "The triangle doesn't exist"
}

console.log(square(3, 4, 6))

/////////////////////Завдання 4////////////////////////////////////////
function generation(array, amount) {
    if (amount) {
        let a = []
        array.push(a)
        return generation(a, amount - 1)
    }
}

let q = []

generation(q, 3)

console.log(q)

